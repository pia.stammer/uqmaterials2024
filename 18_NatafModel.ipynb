{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "baa9b854-0b9f-4a24-a6c4-85113726230b",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Uncertainty Quantification\n",
    "\n",
    "#### Prof. Dr. Martin Frank\n",
    "\n",
    "---\n",
    "\n",
    "## **Modeling & Parametrization**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3322724b-1490-4965-a2c5-49825e11e90b",
   "metadata": {},
   "source": [
    "### **18 Nataf Model**\n",
    "<img src=\"img/NatafModel.PNG\" alt=\"image\" width=\"500\" height=\"auto\" style=\"display: block; margin: 0 auto\" >\n",
    "\n",
    "Imagine we have $d$ input random variables $Z_1,\\dots,Z_d$. We ask for their joint pdf $ f(Z_1,\\dots,Z_d)$. Given are the marginals, i.e. the single-variable pdfs\n",
    "\\begin{align*}\n",
    " f_i(z_i)=\\int\\dots\\int f(z_1,\\dots,z_d)dz_1\\dots dz_{i-1} dz_{i+1}\\dots dz_{d},\n",
    "\\end{align*}\n",
    "how and under which conditions can we construct a joint pdf?\n",
    "\n",
    "The Nataf Model is one way of constructing such a joint pdf. In addition we prescribe the covariance $\\mathrm{Cov}(Z_i,Z_j)$.\n",
    "We denote cumulative distribution functions (cdfs) by\n",
    "\\begin{align*}\n",
    "F(z)=\\int_{-\\infty}^{z_1}\\dots\\int_{-\\infty}^{z_d} f(z_1,\\dots,z_d)dz_1\\dots dz_{d}\n",
    "\\end{align*}\n",
    "and $F_i$ for $ f_i$. We assume in the following continuous random variables, i.e. the cdf is continuous, and that the cdf is strictly monotonically increasing so that $F_{i}^{-1}$ exists.\n",
    "Let $\\Phi$ be the cdf of a 1D $\\mathcal{N}(0,1)$ random variable.\n",
    "\n",
    "1. Define $Y_i=\\Phi^{-1}(F_i(Z_i))$. Then $Y_i\\sim\\mathcal{N}(0,1)$.\n",
    "\n",
    "**Proof:**\n",
    "\\begin{align*}\n",
    "P(Y_{i}\\leq y_i)=&P(\\Phi^{-1}(F_i(Z_i))\\leq y_i)=P(F_i(Z_i)\\leq \\Phi(y_i))\n",
    "\\end{align*}\n",
    "because $\\Phi$ is strictly non increasing,\n",
    "\\begin{align*}\n",
    "\\quad \\quad \\quad \\quad \\quad \\quad \\quad \\quad \\quad= P\\left( Z_i\\leq F_{i}^{-1}(\\Phi(y_i))\\right)=F_i\\left( F_{i}^{-1}(\\Phi(y_i)) \\right)=\\Phi(y_i) \\quad \\square\n",
    "\\end{align*}\n",
    "\n",
    "2. Assume that the random variables $Y_i$ are jointly normally distributed as $Y\\sim\\mathcal{N}(0,V)$ with covariance matrix $V$ and $V_{ii}=1$. We are left with choosing the off-diagonal elements so that the back-transformed random variables $Z_i$ have prescribed covariance. One can compute\n",
    "\\begin{align*}\n",
    "& \\mathrm{Cov}(Z_i,Z_j)\\\\ = &\\int_{\\mathbb{R}}\\int_{\\mathbb{R}}(z_i-\\mu_i)(z_j-\\mu_j) f_i(z_i) f_j(z_j)\\times\\frac{\\varphi_2\\left( Y_i(z_i),Y_j(z_j)\\right)}{\\varphi_1\\left( Y_i(z_i)\\right)\\cdot\\varphi_1\\left( Y_j(z_j)\\right)}dz_i dz_j\n",
    "\\end{align*}\n",
    "where $\\varphi_1$ is the standard normal pdf, $\\varphi_2$ is the bivariate normal pdf with covariance matrix \n",
    " \\begin{align*}\n",
    "V= \\begin{bmatrix}\n",
    "  1 & V_{ij}  \\\\\n",
    "  V_{ij} & 1\n",
    " \\end{bmatrix}.\n",
    "\\end{align*}\n",
    "This defines the $V_{ij}$ from the $\\mathrm{Cov}(Z_i,Z_j)$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3e43f3d3-1d63-482f-99d4-84208d8debbf",
   "metadata": {
    "tags": []
   },
   "source": [
    "---\n",
    "### **Exercises**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2f09a6ec-b322-4a59-8630-e592a240873b",
   "metadata": {},
   "source": [
    "**EXERCISE 18.1)**\n",
    "\n",
    "Prove the Nataf covariance formula \n",
    "\n",
    "$$\n",
    "\\begin{aligned}\n",
    "\\text{cov}(Z_i,Z_j) = \\int\\int (z_i-\\mu_i)(z_j-\\mu_j)\\rho_i(z_i)\\rho_j(z_j)\\frac{\\varphi_2(y_i(z_i),y_j(z_j))}{\\varphi_1(y_i(z_i))\\varphi_1(y_j(z_j))}\\,dz_i\\,dz_j,\n",
    "\\end{aligned}\n",
    "$$\n",
    "\n",
    "where $\\varphi_1$ is a standard normal pdf and $\\varphi_2$ is the\n",
    "bivariate normal pdf with covariance matrix \n",
    "\n",
    "$$\n",
    "\\begin{aligned}\n",
    "V = \\begin{pmatrix}\n",
    "1 & V_{ij} \\\\\n",
    "V_{ji} & 1\n",
    "\\end{pmatrix}.\\end{aligned}\n",
    "$$ \n",
    "\n",
    "**Hint:** Use\n",
    "$\\left(f^{-1}\\right)'(a) = \\frac{1}{f'(f^{-1}(a))}$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e8d4e515-a810-498a-b08d-a43be9525256",
   "metadata": {},
   "source": [
    "**Solutions:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ebc0e9bf-df10-42ea-ac52-dd1ea9736ea3",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "#(Run this cell to load solutions)\n",
    "from IPython.display import display, Image\n",
    "display(Image(filename='solutions/18_1.PNG',width=600))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
