{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "baa9b854-0b9f-4a24-a6c4-85113726230b",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Uncertainty Quantification\n",
    "\n",
    "#### Prof. Dr. Martin Frank\n",
    "\n",
    "---\n",
    "\n",
    "## **Modeling & Parametrization**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3322724b-1490-4965-a2c5-49825e11e90b",
   "metadata": {},
   "source": [
    "### **24 Karhunen-Lo&#232;ve (KL) expansion**\n",
    "The KL expansion is an orthonormal decomposition like in the previous section, only in function space.\n",
    "Consider a random variable that depends on a continuous variable, here time \n",
    "$$Z_t(\\omega),\\, \\omega\\in\\Omega$$\n",
    "instead of $Z_i(\\omega)$, $i=1,\\dots,d$.\n",
    "\n",
    "Statistical quantities are defined by integration over $\\Omega$, i.e. \n",
    "$$\\mathrm{Cov}(Z_t,Z_s)=\\int_{\\Omega} Z_t(\\omega)Z_s(\\omega)f (\\omega)d\\omega $$\n",
    "We write $C(t,s)=\\mathrm{Cov}(Z_t,Z_s)$ and consider it a function of two variables $C:[0,T]\\times [0,T]\\rightarrow \\mathbb{R}$.\n",
    "\n",
    "This function defines an operator on the space of time-dependent functions\n",
    "$$(Cu)(t)=\\int_0^T C(t,s)u(s)ds$$\n",
    "Then, under mild assumptions on $C(t,s)$, we get:\n",
    "\n",
    "- $C$ is symmetric with respect to the inner product\n",
    "$$\n",
    "(u,v)=\\int_0^T u(t)v(t)dt,\n",
    "$$\n",
    "i.e.\n",
    "\\begin{align*}\n",
    "(Cu,v)&=\\int_0^T (Cu)(t)v(t)dt= \\int_0^T\\int_0^T C(t,s)u(s)ds v(t)dt\\\\\n",
    "&= \\int_0^T\\int_0^T C(t,s)u(s)v(t)dsdt=\\int_0^T\\int_0^T C(s,t)v(t)u(s)dtds\\\\\n",
    "&=\\int_0^T (Cv)(s)u(s)ds=(Cv,u)=(u,Cv)\n",
    "\\end{align*}\n",
    "- $C$ is Hilbert-Schmidt, i.e. it can be diagonalized with orthonormal eigenfunctions and real eigenvalues. Furthermore, all eigenvalues are positive.\n",
    "\\begin{align*}\n",
    "\\lambda_1\\geq\\lambda_2\\geq\\dots\\geq0,\\ \\lim_{n\\rightarrow\\infty}\\lambda_n=0\\ \\text{and}\\ \\sum_{i=1}^{\\infty}\\lambda_i^2<\\infty\n",
    "\\end{align*}\n",
    "\n",
    "The eigenfunctions $\\psi_i$ satisfy \n",
    "\\begin{align*}\n",
    "C\\psi_i&=\\lambda_i\\psi_i\\\\\n",
    "(\\psi_i,\\psi_j)&=\\delta_{ij}\n",
    "\\end{align*}\n",
    "We can expand any time-dependent function in terms of this ON basis\n",
    "$$f(t)=\\sum_{j=1}^{\\infty}(f,\\psi_j)\\psi_j(t)$$\n",
    "and the operator $C$ becomes diagonal in the sense\n",
    "$$C(t,s)=\\sum_{j=1}^{\\infty}\\lambda_j\\psi_j(t)\\psi_j(s)$$\n",
    "The KL expansion of $Z_t$ is\n",
    "$$Z_t(\\omega)=\\sum_{j=1}^{\\infty}\\sqrt{\\lambda_j}\\psi_j(t)z_j(\\omega)$$\n",
    "where\n",
    "$$Z_j(\\omega)=\\frac{1}{\\sqrt{\\lambda_j}}\\left(Z_t{(\\omega)},\\psi_j(t)\\right)=\\frac{1}{\\sqrt{\\lambda_j}}\\int_0^TZ_t(\\omega)\\psi_j(t)dt$$\n",
    "\n",
    "**Lemma.** \n",
    "*If* $\\mathrm{E}[Z_t]=0$, *then* $\\mathrm{E}[Z_i]=0$, $\\mathrm{E}[Z_iZ_j]=\\delta_{ij}$. *This means that the new variables* $Z_i$ *are uncorrelated.*\n",
    "\n",
    "**Proof:**\n",
    "Exercise\n",
    "\n",
    "**Lemma.** \n",
    "*The truncated KL expansion* \n",
    "$$Z_t^{(N)}(\\omega)=\\sum_{j=1}^{N}\\sqrt{\\lambda_j}\\psi_j(t)Z_j(\\omega)$$\n",
    "*is optimal in the mean-square sense.*\n",
    "\n",
    "**Proof:**\n",
    "Take any $Y_t,Z_t$. Expand both in terms of the $\\psi_i$ and get\n",
    "\\begin{align*}\n",
    "&\\mathrm{E}\\left[\\int_{0}^{t}\\left( Y_t-Z_t\\right)^2 dt\\right]=\\\\\n",
    "&\\quad =\\int_{\\Omega}\\int_0^T \\left( Y_t(\\omega)-Z_t(\\omega)\\right)^2dtf (\\omega)d\\omega\\\\\n",
    "&\\quad =\\int_{\\Omega}\\int_0^T \\sum_{i,j=1}^{\\infty}\\sqrt{\\lambda_i}\\psi_i(t)\\left( Y_i(\\omega)-Z_i(\\omega)\\right) \\sqrt{\\lambda_j}\\psi_j(t)  \\left( Y_j(\\omega)-Z_j(\\omega)\\right) dtf (\\omega)d\\omega\\\\\n",
    "&\\quad = \\int_{\\Omega} \\sum_i \\sqrt{\\lambda_i}\\sqrt{\\lambda_i}\\left( Y_i(\\omega)-Z_i(\\omega)\\right)^2f (\\omega)d\\omega\n",
    "\\end{align*}\n",
    "If we can only choose $Y_1,\\dots,Y_N$ and $Y_i=0$ for $i>N$, the choice that minimizes the error is \n",
    "$$Y_i=Z_i\\, (i=1,\\dots,N).$$\n",
    "We have an error formula \n",
    "\\begin{align*}\n",
    "\\mathrm{E}\\left[\\int_{0}^{t}\\left( Z_t^{(N)}-Z_t\\right)^2 dt\\right]=\\sum_{i=N+1}^{\\infty}\\lambda^2_i\\int_{\\Omega}[Z_i(\\omega)]^2f (\\omega)d\\omega=\\sum_{i=N+1}^{\\infty}\\lambda^2_i &\\\\\n",
    "& \\quad \\square\n",
    "\\end{align*}\n",
    "\n",
    "**Remark.** The basic idea behind the KL expansion is very simple (yet the method is effective), and it exists in different forms under different names\n",
    "\n",
    "- Truncated singular value decomposition (TSVD)\n",
    "- Principal component analysis (PCA)\n",
    "- Proper orthogonal decomposition (POD).\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3e43f3d3-1d63-482f-99d4-84208d8debbf",
   "metadata": {
    "tags": []
   },
   "source": [
    "---\n",
    "### **Exercises**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6791271e-c8e3-4b8e-9dfc-3e91909c1013",
   "metadata": {},
   "source": [
    "**EXERCISE 24.1)**\n",
    "\n",
    "The KL expansion of the random process $Z_t$ is given by\n",
    "\n",
    "\\begin{equation*}\n",
    "Z_t(\\omega)=\\sum_{j=1}^{\\infty}\\sqrt{\\lambda_j}\\psi_j(t)Z_j(\\omega)\n",
    "\\end{equation*}\n",
    "\n",
    "where\n",
    "\n",
    "\\begin{equation*}\n",
    "Z_j(\\omega)=\\frac{1}{\\sqrt{\\lambda_j}}\\left(Z_t{(\\omega)},\\psi_j(t)\\right)=\\frac{1}{\\sqrt{\\lambda_j}}\\int_0^TZ_t(\\omega)\\psi_j(t)dt.\n",
    "\\end{equation*}\n",
    "\n",
    "Show that if E$[Z_t]=0$, then E$[Z_i]=0$, E$[Z_iZ_j]=\\delta_{ij}$. This means that the new variables $Z_i$ are uncorrelated."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8492a119-bc05-48ab-84f7-fb6a695e9edc",
   "metadata": {},
   "source": [
    "**Solution:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0cd7a0af-1837-4122-9fce-9e5584eef864",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "#(Run this cell to load solution)\n",
    "from IPython.display import display, Image\n",
    "display(Image(filename='solutions/24_1.PNG',width=600))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
