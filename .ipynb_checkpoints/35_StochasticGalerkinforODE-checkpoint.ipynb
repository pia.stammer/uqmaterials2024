{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "baa9b854-0b9f-4a24-a6c4-85113726230b",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Uncertainty Quantification\n",
    "\n",
    "#### Prof. Dr. Martin Frank\n",
    "\n",
    "---\n",
    "\n",
    "## **Stochastic Galerkin**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3322724b-1490-4965-a2c5-49825e11e90b",
   "metadata": {},
   "source": [
    "### **35 Stochastic Galerkin for an ODE**\n",
    "\n",
    "**Example.** (ODE)\n",
    "$$\n",
    "\\begin{array}{l}\n",
    "\\frac{d}{dt}u(t,Q)+Qu(t,Q)&=0\\\\\n",
    "u(0,Q)-\\bar{u}&=0\n",
    "\\end{array}\n",
    "\\quad \\Bigg\\}\\quad \\mathcal{E}(u(Q),Q)=0\n",
    "$$\n",
    "Here, $Q\\sim\\mathcal{N}(\\mu,\\sigma^2)$. First, let $Z\\sim\\mathcal{N}(0,1)$, and we expand every quantity in terms of $Z$:\n",
    "\\begin{align*}\n",
    "Q&=\\mu+\\sigma Z\\\\\n",
    "u(t,Q)&\\approx u^{(N)}(t,Z)=\\sum_{i=0}^{N}u_i(t)\\psi_i(Z)\n",
    "\\end{align*}\n",
    "where $\\psi_i$ are Hermite polynomials.\n",
    "The Galerkin system is\n",
    "\\begin{align*}\n",
    "&\\mathrm{E}\\Big[\\Big(\\overbrace{\\frac{d}{dt}u^{(N)}(t,Z)+Q(Z)u^{(N)}(t,Z)}^{r^{(N)}(t,Z)}\\Big)\\psi_j(Z)\\Big]\\quad \\quad j=0,\\dots,N\\\\\n",
    "&\\mathrm{E}\\left[\\left(\\frac{d}{dt}\\sum_{i=0}^{N}u_i(t)\\psi_i(Z)+Q(Z)\\sum_{i=0}^{N}u_i(t)\\psi_i(Z)\\right)\\psi_j(Z)\\right]=0\\\\\n",
    "&\\frac{d}{dt}\\sum_{i=0}^{N}u_i(t)\\underbrace{\\mathrm{E}\\left[\\psi_i(Z)\\psi_j(Z)\\right]}_{\\gamma_i\\delta_{ij}}+\\sum_{i=0}^{N}u_i(t)\\underbrace{\\mathrm{E}\\left[Q(Z)\\psi_i(Z)\\psi_j(Z)\\right]}_{=?}=0\n",
    "\\end{align*}\n",
    "For the last term we have \n",
    "\\begin{align*}\n",
    "\\mathrm{E}\\left[Q(Z)\\psi_i(Z)\\psi_j(Z)\\right]&=\\mathrm{E}\\left[(\\mu+\\sigma Z)\\psi_i(Z)\\psi_j(Z)\\right]\\\\\n",
    "&=\\mathrm{E}\\left[\\left(\\mu\\psi_0(Z)+\\sigma\\psi_1(Z)\\right)\\psi_i(Z)\\psi_j(Z)\\right]\n",
    "\\end{align*}\n",
    "we can use the following explicit formula for Hermite polynomials<a name=\"cite_ref-2\"></a>[<sup>[2]</sup>](#cite_note-2)\n",
    "\\begin{align*}\n",
    "e_{ijk}&=\\mathrm{E}\\left[\\psi_i(Z)\\psi_j(Z)\\psi_k(Z)\\right]\\\\\n",
    "&=\\left\\{\n",
    "\\begin{aligned}\n",
    "\\frac{i!j!k!}{(s-i)!(s-j)!(s-k)!}&,\\quad \\text{if}\\ i+j+k=2s\\ \\text{is even and}\\ s\\leq i,j,k\\\\\n",
    "0&,\\quad \\text{else}\n",
    "\\end{aligned}\n",
    "\\right.\n",
    "\\end{align*}\n",
    "Furthermore, for Hermite polynomials $\\gamma_i=i!$.\n",
    "Thus\n",
    "\\begin{align*}\n",
    "\\mathrm{E}\\left[\\left(\\mu\\psi_0(Z)+\\sigma\\psi_1(Z)\\right)\\psi_i(Z)\\psi_j(Z)\\right]&=\\mu\\mathrm{E}\\left[\\psi_0(Z)\\psi_i(Z)\\psi_j(Z)\\right]+\\sigma\\mathrm{E}\\left[\\psi_1(Z)\\psi_i(Z)\\psi_j(Z)\\right]\\\\\n",
    "&=\\mu e_{0,j,k}+\\sigma e_{1,j,k}:=a_{ij}\n",
    "\\end{align*}\n",
    "The equations obtained from the Galerkin method read\n",
    "\\begin{align*}\n",
    "\\gamma_j\\frac{d}{dt}u_j(t)+\\sum_{i=0}^{N}a_{ij}u_i(t)=0.\n",
    "\\end{align*}\n",
    "As a system \n",
    "\\begin{align*}\n",
    "\\begin{bmatrix}\n",
    "    \\gamma_0 & & \\\\\n",
    "    & \\ddots & \\\\\n",
    "    & & \\gamma_N\n",
    "  \\end{bmatrix}\n",
    "  \\frac{d}{dt}\n",
    "  \\begin{bmatrix}\n",
    "    u_0(t) \\\\\n",
    "    \\vdots \\\\\n",
    "    u_N(t)\n",
    "    \\end{bmatrix}\n",
    "    +A   \\begin{bmatrix}\n",
    "    u_0(t) \\\\\n",
    "    \\vdots \\\\\n",
    "    u_N(t)\n",
    "    \\end{bmatrix}\n",
    "    =0\n",
    "  \\end{align*}\n",
    "This is an ODE system for the expansion coefficients.\n",
    "We have transformed a scalar ODE with uncertainties into a deterministic system of ODEs for the statistical moments $u_j$.\n",
    "\n",
    "For the initial condition, which is deterministic, we get\n",
    "\\begin{align*}\n",
    "\\mathrm{E}\\left[\\left(\\frac{d}{dt}\\sum_{i=0}^{N}u_i(0)\\psi_i(Z)-\\bar{u}\\right)\\psi_j(Z)\\right]=0,\\quad \\quad j &= 0,\\dots,N\\\\\n",
    "\\sum_{i=0}^{N}u_i(0)\\underbrace{\\mathrm{E}\\left[\\psi_i(Z)\\psi_j(Z)\\right]}_{\\gamma_i\\delta_{ij}}-\\underbrace{\\mathrm{E}\\left[\\bar{u}\\psi_j(Z)\\right]}\\limits_{={\\bar{u}\\mathrm{E}\\left[1\\psi_j(Z)\\right] = \\bar{u}\\mathrm{E}\\left[\\psi_0(Z)\\psi_j(Z)\\right]}} &= 0\\\\\n",
    "u_j(0)\\gamma_j-\\bar{u}\\gamma_0\\delta_{ij} &= 0,\\quad \\quad j=0,\\dots,N\n",
    "\\end{align*}\n",
    "Thus $u_0(0)=\\bar{u}$ and $u_j(0)=0$ for $j =0,\\dots,N$. In vector form\n",
    "\\begin{align*}\n",
    "  \\begin{bmatrix}\n",
    "    u_0(0) \\\\\n",
    "    \\vdots \\\\\n",
    "    u_N(0)\n",
    "    \\end{bmatrix}\n",
    "    =  \\begin{bmatrix}\n",
    "    \\bar{u} \\\\\n",
    "    0\\\\\n",
    "    \\vdots \\\\\n",
    "    0\n",
    "    \\end{bmatrix} \n",
    "  \\end{align*}\n",
    "  \n",
    "---\n",
    "<a name=\"cite_note-2\"></a>[2](#cite_ref-2): Mind that there are different normalizations for the Hermite polynomials in the literature."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3e43f3d3-1d63-482f-99d4-84208d8debbf",
   "metadata": {
    "tags": []
   },
   "source": [
    "---\n",
    "### **Exercises**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e66fbcaa-a927-4357-ae2a-1f0a3540a868",
   "metadata": {},
   "source": [
    "**EXERCISE 35.1)**\n",
    "\n",
    "Consider the ODE with uncertain parameter and uncertain initial condition\n",
    "\n",
    "\\begin{equation*}\n",
    "\\frac{du}{dt}(t,Z) = -\\alpha(Z)u(t,Z),\\quad u(0,Z) = \\beta(Z).\n",
    "\\end{equation*}\n",
    "\n",
    "(a) Expand the random variables $\\alpha$ and $\\beta$ in terms of gPC Hermite polynomials $H_i$. \n",
    "\n",
    "(b) Derive the stochastic Galerkin system by expanding the solution in terms of $H_i$ and testing with $H_k$.\n",
    "\n",
    "(c) How can you obtain the expectation and the variance of the solution $u(t,Z)$?\n",
    "\n",
    "(d) Assume that $\\beta$ is Gaussian normal distributed and $\\alpha$ is deterministic (independent of $Z$). How does the Galerkin system look like? Can you say something about the distribution of $u(t,Z)$?\n",
    "\n",
    "(e) Assume that $\\alpha$ is Gaussian normal distributed and $\\beta$ is deterministic (independent of $Z$). How does the Galerkin system look like? Can you say something about the distribution of $u(t,Z)$?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "43473e74-b98e-4778-b12c-6af3af493724",
   "metadata": {
    "tags": []
   },
   "source": [
    "**Solutions:**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e39e6fc8-3758-47ab-989b-daf6b6846efb",
   "metadata": {},
   "source": [
    "**a)**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bb595a4e-331e-4451-9d73-d9d6227ea664",
   "metadata": {},
   "outputs": [],
   "source": [
    "#(Run this cell to load solution to a)\n",
    "from IPython.display import display, Image\n",
    "display(Image(filename='solutions/35_1_a.PNG',width=600))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b24dcd28-aca6-474a-9060-379eba813cc7",
   "metadata": {},
   "source": [
    "**b)**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f2577198-d994-4b6a-94c8-ad2f6de2a79a",
   "metadata": {},
   "outputs": [],
   "source": [
    "#(Run this cell to load solution to c)\n",
    "from IPython.display import display, Image\n",
    "display(Image(filename='solutions/35_1_b.PNG',width=600))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3fc5a89b-b77f-4485-bb5f-e81d0debd589",
   "metadata": {},
   "source": [
    "**c)**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ac0ab99f-76b1-4fbe-a19f-741d840a9291",
   "metadata": {},
   "outputs": [],
   "source": [
    "#(Run this cell to load solution to c)\n",
    "from IPython.display import display, Image\n",
    "display(Image(filename='solutions/35_1_c.png',width=600))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e926e395-ffc8-4b95-88f1-b4aedda3e809",
   "metadata": {},
   "source": [
    "**d)**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "10884ebd-2c8b-44b3-b316-9d19ea877b74",
   "metadata": {},
   "outputs": [],
   "source": [
    "#(Run this cell to load solution to c)\n",
    "from IPython.display import display, Image\n",
    "display(Image(filename='solutions/35_1_d.PNG',width=600))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6d9e509a-b94a-4819-af2b-8e983b289b3c",
   "metadata": {},
   "source": [
    "**e)**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "67e6188a-c507-4122-bf15-fd4a25ce8c38",
   "metadata": {},
   "outputs": [],
   "source": [
    "#(Run this cell to load solution to c)\n",
    "from IPython.display import display, Image\n",
    "display(Image(filename='solutions/35_1_e.PNG',width=600))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6547b7b3-d247-4746-8967-e36213b380ae",
   "metadata": {},
   "source": [
    "**EXERCISE 35.2)**\n",
    "\n",
    "Recall the ODE with uncertainties from the last exercise:\n",
    "\n",
    "\\begin{equation*}\n",
    "\\frac{du}{dt}(t,Z) = -\\alpha(Z)u(t,Z),\\quad u(0,Z) = \\beta(Z).\n",
    "\\end{equation*}\n",
    "\n",
    "Assume that $\\alpha$ is Gaussian normal distributed and $\\beta$ is deterministic (independent of Z).\n",
    "\n",
    "(a) Derive and solve the Stochastic Galerkin system numerically. Fill in the gaps below to plot the expectation value and variance of $u(t,Z)$ as a function of the gPC approximation order $N$. What do you observe when you increase the time $t$?\n",
    "\n",
    "*Hint*: For Hermite polynomials, $E[H_k^2]=k!$ and $$E[H_iH_jH_k] = e_{ijk} = \\frac{i!j!k!}{(s-i)!(s-j)!(s-k)!}$$ for $2s := i+j+k$ even and $s \\geq i,j,k$ "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5727c749-e55d-4741-a6a8-c7dd53cf9451",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import scipy.integrate as integrate\n",
    "import matplotlib.pyplot as plt\n",
    "import math\n",
    "import scipy.linalg\n",
    "import scipy.special\n",
    "## Function definitions ## \n",
    "\n",
    "def CoeffsAlpha(N):\n",
    "  mu = 0\n",
    "  sigma = 1\n",
    "  a = np.zeros((N+1,1))\n",
    "  a[0]=mu\n",
    "  a[1]=sigma\n",
    "  return a\n",
    "\n",
    "def CoeffsBeta(N):\n",
    "  mu=1\n",
    "  b = np.zeros((N+1,1))\n",
    "  b[0]=mu\n",
    "  return b\n",
    "\n",
    "def epsilon(i,j,k):\n",
    "  y=0\n",
    "  if ((i+j+k)%2)==0:\n",
    "    s=int((i+j+k)/2)\n",
    "    if (s>=i and s>=j and s>=k):\n",
    "      y = math.factorial(i)*math.factorial(j)*math.factorial(k)/math.factorial(s-i)/math.factorial(s-j)/math.factorial(s-k)\n",
    "  return y\n",
    "\n",
    "def gamma(k):\n",
    "  return math.factorial(k)\n",
    "\n",
    "\n",
    "CoeffsAlpha=np.vectorize(CoeffsAlpha)\n",
    "CoeffsBeta=np.vectorize(CoeffsBeta)\n",
    "epsilon=np.vectorize(epsilon)\n",
    "gamma=np.vectorize(gamma)\n",
    "\n",
    "##  Compute Expectation and Standard deviation ##\n",
    "\n",
    "#gPC order\n",
    "Nmax=10\n",
    "#Time\n",
    "t=1\n",
    "\n",
    "Expectation = np.empty((Nmax,))\n",
    "StdDev = np.empty((Nmax,))\n",
    "\n",
    "#Plot for different order\n",
    "for N in range(1,Nmax+1):\n",
    "  #Assemble SG matrix\n",
    "  a = CoeffsAlpha(N)\n",
    "  A = np.zeros((N+1,N+1))\n",
    "\n",
    "  for j in range(N+1):\n",
    "    for k in range(N+1):\n",
    "      A[j,k]=0\n",
    "      for i in range(N+1):\n",
    "        A[j,k] = A[j,k] - 1/gamma(k)*a[i]*epsilon(i,j,k)\n",
    "  \n",
    "  b = CoeffsBeta(N)\n",
    "  u = np.dot(scipy.linalg.expm(t*A.transpose()),b)\n",
    "\n",
    "  #Fill in formulas for expectation value and standard deviation\n",
    "  Expectation[N-1] = #...\n",
    "  StdDev[N-1] = #...\n",
    " \n",
    "\n",
    "plt.figure(figsize=(8,5))\n",
    "plt.plot(np.arange(1,Nmax+1),Expectation,color='blue',label=r'$\\mu$')\n",
    "plt.plot(np.arange(1,Nmax+1),Expectation+StdDev,color='red',label=r'$\\mu \\pm \\sigma$')\n",
    "plt.plot(np.arange(1,Nmax+1),Expectation-StdDev,color='red')\n",
    "plt.title(\"Expectation value and variance as a function of gPC order\")\n",
    "plt.xlabel(\"Order N\")\n",
    "_=plt.legend(shadow=True, fancybox=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b154878d-8035-45ee-ae2e-99b3ca9bef13",
   "metadata": {},
   "source": [
    "**Solution:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b1118666-e6f3-436a-9d82-a5237530c453",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Run this cell to load possible solution for missing lines\n",
    "%load ./solutions/Sol352a.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1543fc4e-2cf0-457f-ace3-cb665f434e54",
   "metadata": {},
   "source": [
    "(b) Select a grid in $Z$ and derive a reconstruction of $u(t,Z)$ by interpolation. What do you obtain for the derived quantities like the expectation value? Solve the Stochastic Collocation system numerically. (Fill in the gaps or adjust the code below according to your choices/derivations)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cb279e81-dbf9-4055-b2c7-1457dcbcb53c",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Normal distribution\n",
    "def f(x,mu,sigma):\n",
    "  return 1/np.sqrt(2*math.pi)*np.exp(-(x-mu)**2/2/sigma)\n",
    "\n",
    "#ODE parameters\n",
    "t=1.0\n",
    "beta=1.0\n",
    "\n",
    "#Quadrature tolerance \n",
    "QuadTol = 1e-10\n",
    "\n",
    "#Gaussian parameters\n",
    "mu=0.0\n",
    "sigma=1\n",
    "\n",
    "#Grid\n",
    "Nmax=10\n",
    "alphal=mu-4*sigma\n",
    "alphar=mu+4*sigma\n",
    "\n",
    "Expectation = np.zeros((Nmax,))\n",
    "StdDev = np.zeros((Nmax,))\n",
    "\n",
    "for N in range(1,Nmax+1):\n",
    "  alpha = np.linspace(alphar,alphal,N)    #Swap upper and lower bound, so that upper bound is chosen for N=1\n",
    "  #TO DO:\n",
    "  #Fill in Solution\n",
    "  y = #...\n",
    "  #Fill in Interpolation\n",
    "  p = #...\n",
    "  \n",
    "  #Expectation\n",
    "  def fun_exp(x):\n",
    "    return np.multiply(np.polyval(p,x),f(x,mu,sigma))\n",
    "  Expectation[N-1],_ = integrate.quadrature(fun_exp,alphal,alphar,tol=QuadTol)\n",
    "  #Variance\n",
    "  def fun_var(x):\n",
    "    return np.multiply((np.polyval(p,x) - Expectation[N-1])**2,f(x,mu,sigma))\n",
    "  StdDev[N-1],_ = np.sqrt(integrate.quadrature(fun_var,alphal,alphar,tol=QuadTol))\n",
    "    \n",
    "plt.figure(figsize=(8,5))\n",
    "plt.plot(np.arange(1,Nmax+1),Expectation,color='blue',label=r'$\\mu$')\n",
    "plt.plot(np.arange(1,Nmax+1),Expectation+StdDev,color='red',label=r'$\\mu \\pm \\sigma$')\n",
    "plt.plot(np.arange(1,Nmax+1),Expectation-StdDev,color='red')\n",
    "plt.title(\"Expectation value and variance as a function of collocation points\")\n",
    "plt.xlabel(\"Collocation points N\")\n",
    "_=plt.legend(shadow=True, fancybox=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d3a43c9b-bbff-46cd-be95-501d11061075",
   "metadata": {},
   "source": [
    "**Solution:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fc5fd879-942c-4bc6-8768-a46ba1f0101c",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Run this cell to reveal possible solution for missing lines \n",
    "%load ./solutions/Sol352b.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3e755393-487d-43a4-9eac-9ae168fd5267",
   "metadata": {},
   "source": [
    "(c) Compare, also with Monte-Carlo sampling. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9f3addf4-3f47-4175-bf50-637a7bb050ee",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Function definitions\n",
    "\n",
    "#TO DO:\n",
    "#Sampling\n",
    "def InverseTransformSampling(N,mu,sigma):\n",
    "  #\n",
    "  #implement inverse transform sampling with the help of the inverse distribution function Finv\n",
    "  #\n",
    "  return \n",
    "\n",
    "#TO DO:\n",
    "#Inverse distribution function\n",
    "def Finv(y,mu,sigma):\n",
    "  #\n",
    "  return #fill in inverse distribution function of y here\n",
    "\n",
    "#Averaging\n",
    "def avg(x):\n",
    "  return 0.5*(x[1:]+x[0:np.size(x)])\n",
    "\n",
    "#ODE parameters \n",
    "t=1.0\n",
    "beta=1.0\n",
    "#Samples\n",
    "N=np.hstack((np.arange(1e3,1e4+1,1e3),np.arange(2e4,1e5+1,1e4),np.arange(2e5,1e6+1,1e5)))\n",
    "#Gaussian parameters\n",
    "mu=0.0\n",
    "sigma=1\n",
    "\n",
    "Expectation=np.empty((np.size(N),))\n",
    "StdDev=np.empty((np.size(N),))\n",
    "\n",
    "for i in range(np.size(N)):\n",
    "  #Sample\n",
    "  #TO DO:\n",
    "  alpha = #...\n",
    "  #Result\n",
    "  y = #...\n",
    "  Expectation[i] = #...\n",
    "  StdDev[i]= #...\n",
    "\n",
    "plt.figure(figsize=(8,5))\n",
    "plt.semilogx(N,Expectation,color='blue',label=r'$\\mu$')\n",
    "plt.semilogx(N,Expectation-StdDev,color='red',label=r'$\\mu \\pm \\sigma$')\n",
    "plt.semilogx(N,Expectation+StdDev,color='red')\n",
    "plt.title(\"Expectation value and variance as a function of samples\")\n",
    "plt.xlabel(\"Samples N\")\n",
    "_=plt.legend(shadow=True, fancybox=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "05dbd013-a811-4694-a7cf-1938ccf9aeb8",
   "metadata": {},
   "source": [
    "**Solution:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7464d3c3-bdb2-42ec-a2a5-6eb86f532ed2",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Run this cell to reveal code with filled in gaps for c)\n",
    "%load ./solutions/Sol352c.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5da9c2a2-ca51-48a1-a6fc-1f5268d19ee6",
   "metadata": {},
   "source": [
    "&nbsp;\n",
    "\n",
    "**Projects:**\n",
    "\n",
    "Reproduce and possibly extend the results in either of the following papers (you can also use a paper of your choice):\n",
    "\n",
    "- R. Pulch, *Stochastic Galerkin methods for analyzing equilibria of random dynamical systems,* SIAM/ASA J. Uncertainty Quantification 1(1), 408-430 (2013).\n",
    "- R. Pulch and D. Xiu, *Generalised polynomial chaos for a class of linear conservation laws,* J. Sci. Comput. 51(2), 293-312 (2012).\n",
    "- D. Gottlieb and D. Xiu, *Galerkin Method for Wave Equations with Uncertain Coefficients,* Comm. Comput. Phys. 3(2), 505-518 (2008). \n",
    "- D. Xiu and G.E. Karniadakis, *Supersensitivity Due to Uncertain Boundary Conditions,* Int. J. Numer. Meth. Engng. 61(12), 2114-2138 (2004).\n",
    "- D. Xiu and G.E. Karniadakis, *Modeling Uncertainty in Steady State Diffusion Problems via Generalized Polynomial Chaos,* Comput. Methods Appl. Mech. Engrg. 191, 4927-4948 (2002). \n",
    "\n",
    "&nbsp;\n",
    "\n",
    "All papers can be found in ILIAS or via internet search.\n",
    "Use the following general strategy:\n",
    "\n",
    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;(a) Implement an efficient solver for the deterministic problem.  \n",
    "\n",
    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;(b) Implement Monte-Carlo sampling. (Possibly keep it running in the background) Also try quasi-MC or MLMC.\n",
    "\n",
    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;(c) Implement a tensorized quadrature method.\n",
    "\n",
    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;(d) Reproduce the derivation of the stochastic Galerkin system. Implement a solver following the guidelines in each of the papers.\n",
    "\n",
    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;(e) Compare and discuss.\n",
    "\n",
    "\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
