{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "baa9b854-0b9f-4a24-a6c4-85113726230b",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Uncertainty Quantification\n",
    "\n",
    "#### Prof. Dr. Martin Frank\n",
    "\n",
    "---\n",
    "\n",
    "## **Smoothness & Convergence Rates**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3322724b-1490-4965-a2c5-49825e11e90b",
   "metadata": {
    "jp-MarkdownHeadingCollapsed": true,
    "tags": []
   },
   "source": [
    "### **37 Orthogonal Polynomials**\n",
    "\n",
    "The error estimates for non-intrusive methods rely on interpreting these methods as quadrature rules.\n",
    "What (optimal) convergence rate can we expect for a d-dimensional quadrature rule?\n",
    "\n",
    "Orthogonal polynomials satisfy\n",
    "\\begin{align*}\n",
    "\\left(\\psi_i,\\psi_j\\right)_{f}=\\gamma_i\\ \\delta_{ij}\n",
    "\\end{align*}\n",
    "and $\\text{degree}(\\psi_i)=i$ with\n",
    "$$\n",
    "(g,h)_f=\\int f(z)g(z)h(z)dz.\n",
    "$$\n",
    "Sturm-Liouville Theory says that all orthogonal polynomials satisfy a 3-term recursion relation\n",
    "$$\\psi_{n+1}(z)=(A_{n}z+B_{n})\\psi_{n}(z)-C_{n}\\psi_{n-1}(z)$$\n",
    "and a differential equation\n",
    "\\begin{align*}\n",
    "\\underbrace{g_{2}(z)\\psi_{n}^{''}(z)+g_{1}(z)\\psi_{n}^{'}(z)}\\limits_{(L\\psi_n)(z)}=\\lambda_n\\psi_n(z).\n",
    "\\end{align*}\n",
    "More precisely stated: Orthogonal polynomials are eigenfunctions of a second-order (symmetric) differential operator.\n",
    "\n",
    "**Example.** (Legendre)\n",
    "\n",
    "For Legendre polynomials, $f(z)=\\frac{1}{2}$ on $[-1,1]$. The first Legendre polynomials are\n",
    "\\begin{align*}\n",
    "P_0(z)=1,\\ P_1(z)=z,\\ P_2(z)=\\frac{1}{2}(3z^2-1),\\ P_3(z)=\\frac{1}{3}(5z^3-3z) \n",
    "\\end{align*}\n",
    "Recursion relation:\n",
    "\\begin{align*}\n",
    "P_{n+1}(z)=\\frac{2n+1}{n+1}\\ z\\ P_n(z)-\\frac{n}{n+1}P_{n-1}(z)\n",
    "\\end{align*}\n",
    "Differential equation:\n",
    "\\begin{align*}\n",
    "\\underbrace{\\frac{d}{dz}\\left(\\left(1-z^2\\right)\\frac{d}{dz}P_n(z)\\right)}\\limits_{(LP_n)(z)}=-\\underbrace{n(n+1)}\\limits_{\\lambda_n}P_n(z)\n",
    "\\end{align*}\n",
    "The operator $L$ is symmetric because\n",
    "\\begin{align*}\n",
    "(Lg,h)_f&=\\frac{1}{2} \\int_{-1}^{1}\\frac{d}{dz}\\left(1-z^2\\right)\\frac{d}{dz}g\\cdot h dz\\\\\n",
    "&=-\\frac{1}{2} \\int_{-1}^{1}\\left(1-z^2\\right) \\frac{d}{dz}g\\cdot\\frac{d}{dz} h dz\\\\\n",
    "&= (g,Lh)_f\n",
    "\\end{align*}\n",
    "\n",
    "**Example.** (Chebyshev)\n",
    "\n",
    "Weight \n",
    "\\begin{align*}\n",
    "f(z)=\\frac{1}{\\sqrt{1-z^2}}\\quad \\text{on}\\quad ]-1,1[,\\quad \\quad T_n(z)=\\cos(n\\arccos(z))\n",
    "\\end{align*}\n",
    "Recursion relation:\n",
    "$$T_{n+1}(z)=2zT_n(z)-T_{n-1}(z)$$\n",
    "Differential equation:\n",
    "\\begin{align*}\n",
    "\\left(1-z^2\\right)\\frac{d^2}{dz^2}T_{n}(z)-z\\frac{d}{dz}T_n(z)=&-n^2T_n(z)\\\\\n",
    "\\sqrt{1-z^2}\\left[\\frac{d}{dz}\\left(\\sqrt{1-z^2}\\frac{d}{dz}T_n(z)\\right)\\right]=&-n^2T_n(z)\n",
    "\\end{align*}\n",
    "\n",
    "\n",
    "**Remark**.\n",
    "Similar statements hold for trigonometric polynomials $\\sin(nx), \\cos(nx)$ or\n",
    "$$(\\mathrm{e}^{iz})^n=\\mathrm{e}^{inz}=\\cos{(nz)}+\\mathrm{i}\\sin{(z)}$$\n",
    "and\n",
    "$$\\frac{d^2}{dx^2}\\sin{(nx)}=-n^2\\sin{(nx)}$$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3e43f3d3-1d63-482f-99d4-84208d8debbf",
   "metadata": {
    "tags": []
   },
   "source": [
    "---\n",
    "### **Exercises**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a6bbc9b1-2434-4f61-ba77-3e301edfe1be",
   "metadata": {},
   "source": [
    "**EXERCISE 37.1)**\n",
    "\n",
    "In this exercise, we prove the properties of Legendre polynomials that were used in the lecture. \n",
    " \n",
    " We define the Legendre polynomial of degree $n$ by \n",
    " $$ P_n(x) = \\frac{1}{2^n n!} \\frac{d^n}{dx^n}(x^2 -1)^n \\;.$$\n",
    " \n",
    " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (a) Show that $P_n$ is indeed a polynomial of degree $n$.\n",
    " \n",
    " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (b) Show that the leading coefficient (i.e. the coefficient in front of $x^n$) is \n",
    " $$\\frac{(2n)!}{2^n (n!)^2} \\;.$$\n",
    " \n",
    " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (c) Show that the Legendre polynomials are orthogonal with respect to the standard inner product\n",
    " $$(f,g) = \\int_{-1}^1 f(x)g(x) dx \\;,$$\n",
    " \n",
    " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;     more specifically that\n",
    " $$(P_l,P_m) = \\frac{2}{2l+1} \\delta_{lm} \\;.$$\n",
    " \n",
    " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;     *Hint:* Show that for any sufficiently smooth function \n",
    " $$2^n n! (f,P_n) = (-1)^n \\int_{-1}^{1} f^{(n)}(x)(x^2-1)^n dx $$\n",
    " \n",
    " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;     and use \n",
    " $$ \\int_0^1 (1-y)^n y^{-1/2} dy = \\frac{2^{n+1}n!}{1 \\cdot 3 \\cdot \\cdot \\cdot (2n+1)} \\;.$$\n",
    " \n",
    " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (d) Show that the $P_n$ are eigenfunctions of a different operator in the following sense\n",
    " $$\\frac{d}{dx} \\left((1-x^2)\\frac{d}{dx}P_n(x)\\right) = -n (n+1) P_n(x) \\;.$$\n",
    " \n",
    " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;     *Hint:* Denote the left hand side by $g$. Show that $g+n(n+1)P_n$ is a polynomial of degree $n-1$, express it as a linear combination of $P_0,...,P_{n-1}$ and use orthogonality. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "76fbfd78-49f0-4f8f-b2c4-c20aafd2585a",
   "metadata": {},
   "source": [
    "**Solutions:**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "26b3d11c-ed8f-4a41-9a0a-8284e3f1fb40",
   "metadata": {},
   "source": [
    "**a) & b)**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b223947e-2e45-4c07-980a-e6044ce2afe5",
   "metadata": {},
   "outputs": [],
   "source": [
    "#(Run this cell to load solution to a and b)\n",
    "from IPython.display import display, Image\n",
    "display(Image(filename='solutions/37_1_ab.PNG',width=600))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8b68e24c-8b1c-4fe2-a2c6-04f7941ff981",
   "metadata": {},
   "source": [
    "**c)**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2a1c7b88-bd0f-4795-9ef2-0a56e45b5841",
   "metadata": {},
   "outputs": [],
   "source": [
    "#(Run this cell to load solution to c)\n",
    "from IPython.display import display, Image\n",
    "display(Image(filename='solutions/37_1_c.PNG',width=600))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "97609910-0f92-40a2-9413-ebd484a73286",
   "metadata": {},
   "source": [
    "**d)**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fe768243-a5cc-4898-a6ea-dc2b1b990042",
   "metadata": {},
   "outputs": [],
   "source": [
    "#(Run this cell to load solution to d)\n",
    "from IPython.display import display, Image\n",
    "display(Image(filename='solutions/37_1_d.PNG',width=600))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
