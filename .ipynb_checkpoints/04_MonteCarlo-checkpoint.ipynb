{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "85fe12b1-cfce-4437-bbdb-b592dff9d048",
   "metadata": {
    "colab_type": "text",
    "id": "yE8PV-Ghauv3"
   },
   "source": [
    "### Uncertainty Quantification\n",
    "\n",
    "#### Prof. Dr. Martin Frank\n",
    "\n",
    "---\n",
    "\n",
    "## **Monte Carlo & Quasi-Monte Carlo**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "36b74b39-368a-422c-98e9-51997273e740",
   "metadata": {},
   "source": [
    "### **4  Monte Carlo**\n",
    "The aim of uncertainty quantification is to explore the input-to-output map of\n",
    "computational models where the input is uncertain, i.e. it is modeled as a random\n",
    "variable with a given pdf. The output will also be a random variable(s) (QoI).\n",
    "Simplest idea: Just run the simulation many times for randomly selected values of\n",
    "the parameter.\n",
    "\n",
    "<img src=\"img/UQpropagation.png\" alt=\"image\" width=\"400\" height=\"auto\" style=\"display: block; margin: 0 auto\" >\n",
    "\n",
    "\n",
    "    \n",
    "More precisely:\n",
    "- Generate a sample of the input random variable $Z \\in \\mathbb{R}^d$, distributed according\n",
    "to $f$.\n",
    "- How to generate (pseudo) random number is an interesting topic of its own\n",
    "(because what is randomness, really?), and we refer to the literature.\n",
    "- Second, run the model with these parameters. This generates a sample of\n",
    "the output.\n",
    "- Third, compute the QoIs from this sample, e.g,\n",
    "\n",
    " \\begin{equation} \\mathrm{E}\\left[u(Z)\\right] \\approx \\frac{1}{N} \\sum_{i=1}^N u\\left(Z^{(i)}\\right) \\end{equation}\n",
    "\n",
    "\n",
    "A QoI typically looks like\n",
    "\n",
    "$$ \\mathcal{R}\\left(u(Z)\\right) =  \\int_{[0,1]^d} \\underbrace{u(z) f(z)}_{g(z)} dz  $$\n",
    "\n",
    "Therefore, we interpret the whole process (sample-solve model-compute QoI) as\n",
    "a numerical integration (quadrature). Let’s assume that $Z^{(i)}$ is a sequence of iid\n",
    "(independently identically distributed) random numbers. Then\n",
    "\n",
    "$$ \\int_{[0,1]^d} u(z) f(z) dz \\approx \\sum_{i=1}^N u\\left(Z^{(i)}\\right) $$\n",
    "\n",
    "Now we study convergence:\n",
    "\n",
    "**Theorem**. *(Weak Law of Large Numbers)*\n",
    "\n",
    "*Let $Z^{(i)} \\subseteq \\mathbb{R}^d$ be a sequence of iid random variables with finite expected value $E\\left[Z^{(i)}\\right]=\\mu$ and finite variance $\\mathrm{Var}\\left[Z^{(i)} \\right]=\\sigma^2$. Define*\n",
    "$$ \\bar{Z}^{(N)} :=  \\frac{1}{N} \\sum_{i=1}^N  Z^{(i)} $$\n",
    "*Then as* $N\\rightarrow \\infty$, $\\bar{Z}^{(N)}$ *converges to* $\\mu$ *in probability, i.e., for every* $\\epsilon > 0$ \n",
    "$$\\lim_{N \\to \\infty} \\mathcal{P}\\left( |\\bar{Z}^{(N)} - \\mu | \\geq \\epsilon \\right) = 0. $$\n",
    "\n",
    "\n",
    "**Theorem.** *(Central Limit Theorem)* \n",
    "\n",
    "*In the same setting as before consider*\n",
    "$$ X^{(N)} := \\frac{\\bar{Z}^{(N)} - \\mu}{\\sigma/\\sqrt{N}}. $$\n",
    "*Then* $X^{(N)}$ *converges in distribution to* $X \\sim \\mathcal{N}(0,1)$ *(* $X$ *distributes according to a standard normal distribution), i.e. the cdf of* $X^{(N)}$ *converges to the cdf of* $X$.\n",
    "\n",
    "*We can conclude that the Monte Carlo method converges.*"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aaff8fc8-5bce-4860-944a-e25864c55053",
   "metadata": {},
   "source": [
    "---\n",
    "### **Exercises**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "04bab743",
   "metadata": {
    "solution2": "hidden",
    "solution2_first": true
   },
   "source": [
    "**EXERCISE 4.1)**\n",
    "\n",
    "Let $X_1,\\dots,X_n$ be iid random variables with $E(X_i)=\\mu$. Show that\n",
    "\n",
    "$$ \\bar X = \\frac{1}{n}\\sum_{i=1}^n X_i $$\n",
    "satisfies \n",
    "$$\\mathrm{E}\\left[\\bar X\\right] = \\mu.$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0c601ff5-40d5-4f6e-b64d-cb31af710efd",
   "metadata": {},
   "source": [
    "\n",
    "**EXERCISE 4.2)**\n",
    "\n",
    "Use the Chebyshev's Lemma to prove the Weak Law of Large Numbers.\n",
    "\n",
    "&nbsp;\n",
    "\n",
    "*Chebyshev's Lemma:* \n",
    "\n",
    "Let $X$ be a random variable with mean $\\mu$ and variance $\\sigma^2$. Then for any real number $r$ we have \n",
    "&nbsp;\n",
    "\n",
    "$$ P(| X - \\mu | \\geq r\\sigma) \\leq \\frac{1}{r^2}.$$\n",
    "\n",
    "&nbsp;\n",
    "\n",
    "**EXERCISE 4.3)**\n",
    "\n",
    "Consider the random ODE\n",
    "\n",
    "$$ \\frac{du}{dt}(t,Z) = \\alpha(Z) u(t,Z), \\;\\;\\; u(0,Z) = \\beta(Z).$$\n",
    "\n",
    "Assume that $\\alpha$ is uniformly distributed on the interval $[a,b]$, and that $\\beta$ is Gaussian normal distributed.\n",
    "\n",
    "&nbsp;\n",
    "\n",
    "&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  (a) Compute the expected value and the variance of $u(t,Z)$ at a fixed time $t$.\n",
    "\n",
    "&nbsp;\n",
    "\n",
    "&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  (b) Use Python to approximate the expectation value of $u(t,Z)$ by Monte-Carlo sampling.         \n",
    "\n",
    "&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  How does the approximation behave with the sample size?\n",
    "\n",
    "&nbsp;\n",
    "\n",
    "&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  (c) Approximate also the standard deviation of $u(t,Z)$.\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
