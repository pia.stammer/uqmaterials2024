{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "baa9b854-0b9f-4a24-a6c4-85113726230b",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Uncertainty Quantification\n",
    "\n",
    "#### Prof. Dr. Martin Frank\n",
    "\n",
    "---\n",
    "\n",
    "## **Multi-level Monte Carlo**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "30d7ea12-cc70-48bf-b856-c0aada4940a3",
   "metadata": {
    "tags": []
   },
   "source": [
    "### **8 Control Variates**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3322724b-1490-4965-a2c5-49825e11e90b",
   "metadata": {},
   "source": [
    "In UQ we consider random input $Z\\in\\mathbb{R}^d$ into some (computational) model. The model response (data-to-solution map) is $u(Z)$. We are typically interested in QoIs related to $u(Z)$, most often $\\mathrm{E}[u(Z)],\\mathrm{Var}[u(Z)]$. In most cases, these quantities are integrals\n",
    "\\begin{align*}\n",
    "\\mathrm{E}[u(Z)]=\\int_{[0,1]^d}\\underbrace{u(z)f(z)}_{g(z)} dz\n",
    "\\end{align*}\n",
    "In the simplest MC experiment, we take random samples $Z^{(i)}$ of the input, compute the corresponding output $u\\left( Z^{(i)}\\right)$, and take e.g. the empirical mean as estimate for the expectation value \n",
    "\\begin{align*}\n",
    "\\mathrm{E}[u(Z)]\\approx \\frac{1}{N}\\sum_{i=1}^{N} u\\left( Z^{(i)}\\right)\n",
    "\\end{align*}\n",
    "We have analyzed this MC experiment by interpreting it as a quadrature rule. Using $[0,1]^d$ and $Z^{(i)}\\sim \\mathcal{U}\\left([0,1]^d\\right)$ we derived the error formula\n",
    "\\begin{align*}\n",
    "\\mathrm{E}\\left[\\left(\\varepsilon_N(g)\\right)^2\\right]^{\\frac{1}{2}}= \\frac{\\sqrt{V(g)}}{\\sqrt{N}},\n",
    "\\end{align*}\n",
    "where the error is\n",
    "\\begin{align*}\n",
    "\\varepsilon_N(g)= Qg-\\overline{Q}_{N}g=\\int_{[0,1]^d}g(z)dz-\\frac{1}{N}\\sum_{i=1}^{N}g\\left( z^{(i)}\\right)\n",
    "\\end{align*}\n",
    "and the variance of the function $g$ is defined as \n",
    "$$\n",
    "V(g) = \\int_{[0,1]^d} (g-Qg)^2 dz.\n",
    "$$\n",
    "\n",
    "We used quasi-random numbers to improve the $\\frac{1}{\\sqrt{N}}$ term to $(\\log N)^d\\frac{1}{N}$. In this lecture, we look at the variance $V(g)$, especially at variance reduction techniques, of which there are many in the literature.\n",
    "\n",
    "The idea of control variates is to approximate the function $g$ by a function $h$ with a known, easy to compute integral\n",
    "\\begin{align*}\n",
    "\\int_{[0,1]^d}g(z)dz=\\int_{[0,1]^d}(g(z)-h(z))dz+\\int_{[0,1]^d}h(z)dz\n",
    "\\end{align*}\n",
    "We apply MC to approximate\n",
    "\\begin{align*}\n",
    "\\int_{[0,1]^d}(g(z)-h(z))dz\n",
    "\\end{align*}\n",
    "The error depends on $V(g-h)$ which is hopefully smaller than $V(g)$.\n",
    "\n",
    "<img src=\"img/Controlvariates.jpg\" alt=\"image\" width=\"400\" height=\"auto\" style=\"display: block; margin: 0 auto\" >\n",
    "\n",
    "\n",
    "Better yet, take $(g-\\lambda h)$ where $\\lambda\\in\\mathbb{R}$ is to be determined\n",
    "\\begin{align*}\n",
    "V(g-\\lambda h)=\\int\\left((g-\\lambda h)-(Qg-\\lambda Qh)\\right)^2dz\\longrightarrow \\min\n",
    "\\end{align*}\n",
    "and\n",
    "\\begin{align*}\n",
    "\\frac{d}{d\\lambda}V(g-\\lambda h)&=2\\int\\left((g-\\lambda h-Qg+\\lambda Qh)(-h+Qh)\\right) dz\\\\\n",
    "&=-2\\underbrace{\\int(g-Qg)(h-Qh)dz}_{\\mathrm{Cov}(f,h)}+2\\lambda\\underbrace{\\int(h-Qh)^2dz}_{\\mathrm{Var}(h)}\\\\\n",
    "&=-2\\mathrm{Cov}(g,h)+2\\lambda V(h)\\\\\n",
    "&\\stackrel{!}{=}0\n",
    "\\end{align*}\n",
    "Thus $\\lambda^*=\\frac{\\mathrm{Cov}(g,h)}{V(h)}$ is the optimal choice. Because $\\lambda=0$ is allowed, we have that $V(g-\\lambda^* h) \\leq V(g)$.\n",
    "\n",
    "**Remark.** In practice, the $\\mathrm{Cov}(g,h)$ is estimated statistically during the MC run.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3e43f3d3-1d63-482f-99d4-84208d8debbf",
   "metadata": {
    "tags": []
   },
   "source": [
    "---\n",
    "### **Exercises**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f59af77f-07d4-4689-8b63-1c020aad7411",
   "metadata": {},
   "source": [
    "**EXERCISE 8.1)**\n",
    "\n",
    "We want to compute the expected value of $g(Z)=\\sqrt{Z} sin(2\\pi Z)$, where $Z$ is uniformly distributed in [0,1], with the help of Monte-Carlo. We use control variates to speed up the computation. \n",
    "\n",
    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   (a) Plot $g$ and the control variate $h$ by running the following block of code. Choose different values of $\\lambda$, how does it affect $h$ and what do you think would be a good choice?\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3ad5e1ad-300d-43a6-9493-51d6c1c83132",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import math\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "#Define g and h\n",
    "\n",
    "def g(Z):\n",
    "  y=math.sqrt(Z)*math.sin(2*math.pi*Z)\n",
    "  return y\n",
    "\n",
    "def h(Z):\n",
    "  y=math.sin(2*math.pi*Z)\n",
    "  return y \n",
    "\n",
    "g=np.vectorize(g)\n",
    "h=np.vectorize(h)\n",
    "\n",
    "#Plot g and control variate h\n",
    "lambda_c = 10\n",
    "zGrid = np.linspace(start=0,stop=1,num=1000)\n",
    "plt.figure(figsize=(8,5))\n",
    "plt.plot(zGrid,g(zGrid),label='g')\n",
    "plt.plot(zGrid,lambda_c*h(zGrid),linestyle='--',linewidth=2,label='h')\n",
    "_=plt.legend(shadow=True, fancybox=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "deffcef3-e22b-402a-baf1-53a9c1c42ab3",
   "metadata": {
    "tags": []
   },
   "source": [
    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   (b) Compute the expected value of $g$ by using Monte Carlo with and without control variate. How does the choice of $\\lambda$ affect the solution?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6033eb7d-eb38-4245-ba21-6d6a70c58deb",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "N=5000  #number of samples\n",
    "\n",
    "tmpMC = 0\n",
    "tmpCV = 0\n",
    "\n",
    "expectedMC = np.zeros((N,1))\n",
    "expectedCV = np.zeros((N,1))\n",
    "\n",
    "for i in range(N):\n",
    "  Z=np.random.uniform(0,1,(1,1))\n",
    "\n",
    "  #Your code: Save MC and CV expectation value approximation with i+1 samples in expectedMC(i) and expectedCV(i)\n",
    "  #...\n",
    "  #the function call could look something like this\n",
    "  #expectedMC[i], expectedCV[i], tmpMC, tmpCV = solution_b(tmpMC,tmpCV,Z,lambda_c,i+1) \n",
    "\n",
    "print(\"Expected value is {0}\".format(expectedMC[N-1]))\n",
    "\n",
    "expectedExact = -0.11980826271647446\n",
    "\n",
    "plt.figure(figsize=(8,5))\n",
    "plt.loglog(np.arange(1,N+1),abs(expectedMC-expectedExact),label='MC')\n",
    "plt.loglog(np.arange(1,N+1),abs(expectedCV-expectedExact),label='CV')\n",
    "plt.loglog(np.arange(1,N+1),1/np.sqrt(np.arange(1,N+1)),linestyle='--',linewidth=2,label='slope 1/2')\n",
    "_=plt.legend(shadow=True, fancybox=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b7e9c29e-1c15-49b7-a182-ea7e164bee5b",
   "metadata": {},
   "source": [
    "**Solution:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bc9d8ef8-97b5-4a54-b129-9dece6fe2a1c",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "#Run this cell to show one possible implementation of the missing function\n",
    "%load ./solutions/Sol081.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9ca1199e-250c-44c9-b76f-20d957fab885",
   "metadata": {},
   "source": [
    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   (c) Look at the code below. What is implemented here? How is the value of $\\lambda$ picked?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4774d599-aef5-40c3-9efa-be315c66dbc2",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "def estimateCov (covOld,QgOld,N,Z):\n",
    "  y=(N-1)/N*covOld + 1/N* (g(Z)-QgOld)* (h(Z)-QgOld)\n",
    "  return y\n",
    "\n",
    "#Compute integral with CV and optimal lambda\n",
    "\n",
    "expectedCVL = np.zeros((N,1))\n",
    "tmpCVL = 0\n",
    "covOld = 0.5           #estimated covariance\n",
    "expectedOld = -0.1     #estimated mean\n",
    "varH = 0.5             #exact variance of function h\n",
    "\n",
    "for i in range(N):\n",
    "  Z = np.random.uniform(0,1,(1,1))\n",
    "  cov = estimateCov(covOld, expectedOld, i+1 , Z) #update covariance\n",
    "  lambdaOpt = cov/varH\n",
    "  tmpCVL = tmpCVL + (g(Z) - lambdaOpt*h(Z))\n",
    "  expectedCVL[i] = 1/(i+1)*tmpCVL\n",
    "  covOld = cov\n",
    "  expectedOld = expectedCVL[i]\n",
    "\n",
    "print(\"Expected value is {0}\".format(expectedCVL[N-1]))\n",
    "\n",
    "expectedExact = -0.11980826271647446\n",
    "plt.figure(figsize=(8,5))\n",
    "plt.loglog(np.arange(1,N+1),abs(expectedMC-expectedExact),label='MC')\n",
    "plt.loglog(np.arange(1,N+1),abs(expectedCV-expectedExact),linestyle='--',linewidth=2,label='CV')\n",
    "plt.loglog(np.arange(1,N+1),abs(expectedCVL-expectedExact),linestyle='-.',linewidth=2,label='CVOpt')\n",
    "plt.loglog(np.arange(1,N+1),1/np.sqrt(np.arange(1,N+1)),label='slope 1/2')\n",
    "_=plt.legend(shadow=True, fancybox=True)\n",
    "\n",
    "print(\"Lambda is {0}\".format(lambdaOpt))\n",
    "\n",
    "plt.figure(figsize=(8,5))\n",
    "plt.plot(zGrid,g(zGrid),label='g')\n",
    "plt.plot(zGrid,np.squeeze(lambdaOpt*h(zGrid)),label='$\\lambda_{Opt} \\; h$')\n",
    "plt.plot(zGrid,lambda_c*h(zGrid),linestyle='--',linewidth=2,label='$\\lambda \\; h$')\n",
    "_=plt.legend(shadow=True, fancybox=True)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
