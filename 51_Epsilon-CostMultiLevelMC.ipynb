{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "baa9b854-0b9f-4a24-a6c4-85113726230b",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Uncertainty Quantification\n",
    "\n",
    "#### Prof. Dr. Martin Frank\n",
    "\n",
    "---\n",
    "\n",
    "## **Multi-level Monte Carlo and Monte Carlo again**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3322724b-1490-4965-a2c5-49825e11e90b",
   "metadata": {
    "jp-MarkdownHeadingCollapsed": true,
    "tags": []
   },
   "source": [
    "### **51 $\\varepsilon$-Cost of Multi-level Monte Carlo**\n",
    "\n",
    "Let us now turn to Multilevel Monte-Carlo. Let $h_l$ be a sequence of grid sizes which is nested, in the sense that\n",
    "$$\n",
    "h_l = s^{-1} h_{l-1} \\quad\\text{ for } l=1,\\dots,L\n",
    "$$\n",
    "with $h=h_L$ (chosen as before to reach a certain accuracy) and an integer $s>1$ (think of $s=2$). \n",
    "\n",
    "The idea of MLMC is that we do not estimate $Q_h g$ directly by MC, but rather write it as a telescoping sum and estimate differences between levels\n",
    "$$\n",
    "E[Q_h g] = E[Q_{h_0} g] + \\sum_{l=1}^L E[Q_{h_l}g-Q_{h_{l-1}}g] =: \\sum_{l=0}^L E[\\Delta Q_l g]\n",
    "$$\n",
    "with $\\Delta Q_l = Q_{h_l}g-Q_{h_{l-1}}g$ and $\\Delta Q_0 = Q_{h_0}g$. We estimate each difference $\\Delta Q_l g$ by MC, using $N_l$ nodes, respectively. We denote this estimator by $\\Delta Q_{l,N_l}g$. Altogether, the MLMC estimator is\n",
    "$$\n",
    "Q^{MLMC}_{(h_l),(N_l)} g = \\sum_{l=0}^L \\Delta Q_{l,N_l}g.\n",
    "$$\n",
    "The notation already got tedious\\dots The indexes $(h_l)$ and $(N_l)$ denote that we need sequences of $h_l$ and $N_l$.\n",
    "\n",
    "The equivalent equation to (50.1) has the form\n",
    "\\begin{equation}\n",
    "\\label{eq:MLMCerror}\n",
    "\\tag{51.1}\n",
    "E[( Q^{MLMC}_{(h_l),(N_l)} g- Qg)^2] = \\sum_{l=0}^L N_l^{-1} V(\\Delta Q_l g) + (E[Q_h g]-Qg)^2.\n",
    "\\end{equation}\n",
    "\n",
    "\n",
    "We prove the following abstract MLMC convergence theorem:\n",
    "\n",
    "**Theorem.**\n",
    "*Let* $\\varepsilon<exp(-1)$. *Assume there are positive constants* $\\alpha$, $\\beta$, $\\gamma$, $C_1$, $C_2$, $C_3$ such that $\\alpha\\geq \\frac12 \\min(\\beta,\\gamma)$. *Under the assumptions*\n",
    "\n",
    "*(A1) $\\quad$ The discretization error behaves like* $|Q_{h_l}g-Qg| \\leq C_1 h_l^\\alpha$\n",
    "\n",
    "*(A2) $\\quad$ The variance behaves like* $V(\\Delta Q_l g) \\leq C_2 N_l^{-1} h_l^\\beta$\n",
    "\n",
    "*(A3) $\\quad$ The cost behaves like* $K(\\Delta Q_l)\\leq C_3 N_l h_l^{-\\gamma} $\n",
    "\n",
    "*there exist numbers of samples* $N_l$ ($l=0,\\dots,L$) *such that the RMSE satisfies*\n",
    "$$\n",
    "E[(Q^{MLMC}_{(h_l),(N_l)} g- Qg)^2] < \\varepsilon^2\n",
    "$$\n",
    "*with costs that behave like*\n",
    "$$\n",
    "K(Q^{MLMC}_{(h_l),(N_l)} g) \\leq C\n",
    "\\begin{cases}\n",
    "\\varepsilon^{-2}, &\\text{ if } \\beta>\\gamma,\\\\\n",
    "\\varepsilon^{-2}(\\log\\varepsilon)^2, &\\text{ if } \\beta=\\gamma,\\\\\n",
    "\\varepsilon^{-2-(\\gamma-\\beta)/\\alpha}, &\\text{ if } \\beta<\\gamma.\n",
    "\\end{cases}\n",
    "$$\n",
    "*The constant* $C$ *depends on* $C_1$, $C_2$, $C_3$.\n",
    "\n",
    "\n",
    "**Remark.**\n",
    "- It is possible to use QMC instead of MC. The corresponding theorem can be found in the original source of this chapter. \n",
    "- One can expect that $\\beta = 2\\alpha$, because of the dimensions of the variance compared to the expected value.\n",
    "- In the case $\\beta>\\gamma$, i.e., the variance decreases faster than the cost increases, the dominant computational cost is on the coarsest level. Thus MLMC is as good as MC without the discretization $h$. We are able to \"hide\" the cost of discretization (which would effectively decrease the convergence order of MC).\n",
    "- In the case $\\beta<\\gamma$, the dominant computational cost is on the finest level. Because $h_L^{-\\alpha}=\\mathcal{O}(\\varepsilon)$ we have that the cost on the finest level is $K_L = \\mathcal{O}(\\varepsilon^{-\\gamma/\\alpha})$. On the other hand, if $\\beta=2\\alpha$, the total cost is $\\mathcal{O}(K_L)$, which is the best we can achieve.\n",
    "- In  applications, the challenge is in proving that the assumptions of the theorem are valid. In particular, the parameters in the theorem need to be estimated so that the optimal numbers of samples $N_l$ can be chosen.\n",
    "\n",
    "\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
