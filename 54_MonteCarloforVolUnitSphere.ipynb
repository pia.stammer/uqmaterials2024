{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "baa9b854-0b9f-4a24-a6c4-85113726230b",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Uncertainty Quantification\n",
    "\n",
    "#### Prof. Dr. Martin Frank\n",
    "\n",
    "---\n",
    "\n",
    "## **Multi-level Monte Carlo and Monte Carlo again**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3322724b-1490-4965-a2c5-49825e11e90b",
   "metadata": {
    "jp-MarkdownHeadingCollapsed": true,
    "tags": []
   },
   "source": [
    "### **54 Monte Carlo for the Volume of the Unit Sphere**\n",
    "\n",
    "In an exercise, we have investigated the convergence of the MC method, when approximating the volume of the unit sphere using uniform samples in every dimension. It can be seen that - while the absolute error - looks fine, with $N=10^8$ and $d=20$ the MC result gets no significant digit of the result right (relative error). What's going on?\n",
    "   \n",
    "<img src=\"img/MCdim10.png\" alt=\"image\" width=\"400\" height=\"auto\" style=\"display: block; margin: 0 auto\" >\n",
    "<img src=\"img/MCdim15abs.png\" alt=\"image\" width=\"400\" height=\"auto\" style=\"display: block; margin: 0 auto\" >\n",
    "<p style=\"text-align: center;\">Figure 9: Absolute error of the MC method when approximating the volume of the unit sphere using uniform samples in every dimension. Comparison with convergence rate $1/2$.</p> \n",
    "\n",
    "<img src=\"img/MCd1020.png\" alt=\"image\" width=\"400\" height=\"auto\" style=\"display: block; margin: 0 auto\" >\n",
    "<p style=\"text-align: center;\">Figure 10: Absolute and relative error of the MC method when approximating the volume of the unit sphere using uniform samples in every dimension. Comparison with convergence rate $1/2$.</p> \n",
    "\n",
    "**Example.**\n",
    "Let $A\\subseteq[0,1]^d$ and \n",
    "$$\n",
    "g(z)=\\frac{1}{|A|}\\chi_{A}(z)\n",
    "$$\n",
    "where\n",
    "\\begin{align*}\n",
    "\\chi_{A}(z)=\\left\\{\n",
    "\\begin{aligned}\n",
    "1&,\\quad z\\in A\\\\\n",
    "0&,\\quad z\\not\\in A\n",
    "\\end{aligned}\n",
    "\\right.\\quad \\quad\\text{and}\\quad \\quad |A|=\\text{vol}(A)\n",
    "\\end{align*}\n",
    "\n",
    "By construction,\n",
    "$$\n",
    "\\int_{[0,1]^d}g(z)dz=1\n",
    "$$\n",
    "so that the absolute error is the relative error. The MC error is\n",
    "$$\n",
    "\\varepsilon^2_{RMS}=\\mathrm{E}\\left[\\left( Qg-\\overline{Q}_{N}g \\right)^2\\right]=\\frac{\\mathrm{Var}(g)}{N}\n",
    "$$\n",
    "and\n",
    "\\begin{align*}\n",
    "\\mathrm{Var}(g)&=\\int_{[0,1]^d}\\Big{(} g(z)-\\overbrace{\\int_{[0,1]^d}g(z')dz'}^{=1}\\Big{)}^2 dz\\\\\n",
    "&=\\int_{[0,1]^d}\\left( g(z)-1\\right)^2dz\\\\\n",
    "&=\\int_{A}\\left(\\frac{1}{|A|}-1\\right)^2dz+\\int_{[0,1]^d\\setminus A}(0-1)^2dz\\\\\n",
    "&=|A|\\left(\\frac{1}{|A|}-1\\right)^2+\\left( 1-|A|\\right)\\cdot 1\\\\\n",
    "&=|A|\\left(\\frac{1}{|A|^2}-\\frac{2}{|A|}+1\\right)+1-|A|\\\\\n",
    "&=\\frac{1}{|A|}-2+|A|+1-|A|=\\frac{1}{|A|}-1\n",
    "\\end{align*}\n",
    "\n",
    "In the case of unit sphere $|A|\\rightarrow 0$ as $d\\rightarrow\\infty$ faster than exponentially:\n",
    "$$\n",
    "|A| = \\frac{\\pi^{d/2}}{\\Gamma(d+\\frac12)} = \\frac{\\pi^{d/2}}{(d-\\frac12)(d-\\frac32)\\cdots\\frac12 \\pi^{1/2}}.\n",
    "$$\n",
    "\n",
    "Even simpler: Let\n",
    "$$\n",
    "A=\\prod_{i=1}^{d}\\left[z_i-\\frac{\\Delta}{2},z_i+\\frac{\\Delta}{2}\\right]\n",
    "$$ \n",
    "be a cube with side length $\\Delta$, since $|A|=\\Delta^d$ we get\n",
    "$$\n",
    "\\mathrm{Var}(f)=\\left(\\frac{1}{\\Delta}\\right)^d-1\n",
    "$$\n",
    "Even if $z_i=\\frac{1}{2}$ and $\\Delta=0.999$, $\\mathrm{Var}(g)$ tends to $\\infty$ exponentially fast with increasing $d$.\\\\\n",
    "If\n",
    "$$\n",
    "\\mathrm{Var}(g)\\approx\\left(\\frac{1}{\\Delta}\\right)^d\n",
    "$$\n",
    "then\n",
    "$$\n",
    "\\varepsilon^2\\approx\\frac{1}{N}\\cdot \\left(\\frac{1}{\\Delta}\\right)^d\n",
    "$$\n",
    "and we need to take \n",
    "$$\n",
    "N\\approx\\left(\\frac{1}{\\Delta}\\right)^d\\frac{1}{\\varepsilon^2}.\n",
    "$$\n",
    "In this case, MC suffers from the curse of dimensionality, according to our definition.\n",
    "\n",
    "**Remark.**\n",
    "This behavior can also be understood if we think of MC as a Bernoulli experiment with success probability $p$ (think of a sample point being inside a subset of the unit sphere). \n",
    "When we repeat this experiment, we need on average $\\frac{1}{p}$ trials to have one success. In high dimensions, $p$ might behave like $2^{-d}$. In that case, any MC method must suffer from the curse of dimensionality. "
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
