# UQMaterials2024


## Getting started
To pull these materials into the provided jupyter server follow the steps below:
1. Register to get an account on https://jupyter-test.scc.kit.edu/ 
    - if you join the Ilias group until friday 19.04.2024, this will be taken care of automatically and you can go directly to step 2. 
    - otherwise contact leon.schuhmacher@kit.edu or ask Pia Stammer, Chinmay Patwardhan or Martin Frank about this
2. Register on https://fels.scc.kit.edu/user/register-service.xhtml?ssn=jupyter-test with your KIT account, then log in at https://jupyter-test.scc.kit.edu/ and start "Data science with nbgrader" environment
3. Open a terminal on the server (click big plus sign in top left corner to open launcher, then choose "Terminal" in category "Other") 
4. Run
> pip install pexpect==4.9.0

(this is necessary to enable git clone and authentication)

4. At the top of your window (next to File/Edit/...) go to Git>Clone a Repository and enter link of this repo (https://gitlab.kit.edu/pia.stammer/uqmaterials2024/)
5. Done, the materials are available on your jupyter server!


## Troubleshooting

#### Problems with git
- If push/pull is not working, try reinstalling pexpect (pip install pexpect==4.9.0)
- If you cannot pull because local and remote branch have diverged (e.g. you made some changes in your files and at the same time the git repository was updated try opening the Terminal and running 
   > git pull --rebase 

### Problems with python
- If you get a message that some package is missing, check whether it is imported, otherwise add 
> import [package-name]

If it is imported but not installed run

> pip install [package-name] 

in the terminal or put
> !pip install [package-name] 

at the top of the cell you want to run (before the "import [package-name]" command)

### Markdown/Latex
***Warning***: Some of the markdown cells use this jupyter notebook latex extension: https://jupyter-contrib-nbextensions.readthedocs.io/en/latest/nbextensions/latex_envs/README.html 
If you do not edit the markdown cells they should be displayed fine also without this package. However if you open the editor you will probably not be able to compile them. In this case you can either try to install the extension or just pull the already compiled version from this git repo again.
